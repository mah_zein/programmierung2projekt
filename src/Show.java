import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import javafx.geometry.HPos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * collection of methods
 */
public class Show extends AlbumFX{

    private static Logger loggerShow = Logger.getLogger("Show");

    private static int numImage = 0;
    private static int imageV = 0;
    private static int imageH = 0;
    private static int check;
    private static double puffer;
    private static int col = 6;

    private static List<ColumnConstraints> spalten = new ArrayList<>();

    public static List<Path> imagePath = new ArrayList<>();
    public static List<ImageView> fullImgViewList = new ArrayList<>();
    public static List<Text> imageName = new ArrayList<>();
    public static List<String> infoOfImage = new ArrayList<>();

    public static List<ImageView> imageViewList = new ArrayList<>(); // for fade-in-animation
    public static List<Text> imageNameAgain = new ArrayList<>(); // for fade-in-animation

    /**
     * @param primaryStage main window
     * @return get list of imgaeview objects of images from a directory
     */
    public static List openDirectory(Stage primaryStage){
        DirectoryChooser directoryChooser1 = new DirectoryChooser();
        File selectedDirectory1 = directoryChooser1.showDialog(primaryStage);
        try {
            File[] fileList = selectedDirectory1.listFiles();
            for (File file : fileList) {
                imagePath.add(file.toPath());
                Image newImage = new Image(file.toURI().toString(),
                        AlbumFX.getwSize()/6.5, 230, true, true);
                fullImageList.add(newImage);
                ImageView iViewDirectory = new ImageView(newImage);
                if(check == 0){
                    imageViewList.add(iViewDirectory);
                    fullImgViewList.add(iViewDirectory);
                }
                Text bildtext = new Text(file.toPath().getFileName().toString());
                bildtext.setFill(Color.WHITE);
                bildtext.setOpacity(0);
                bildtext.setFont(new Font(13));
                imageName.add(bildtext);
                imageNameAgain.add(bildtext);
            }
        } catch (NullPointerException e){
            loggerShow.log(Level.SEVERE,"Exception occurred while loading images " + e);
        }
        loggerShow.info("method 'openDirectory' finished");
        return imageViewList;
    }


    /**
     * @param primaryStage main window
     * @return get list of imageview objects of one/many image/s
     */
    public static List openMultiImage(Stage primaryStage){
        FileChooser fileChooser2 = new FileChooser();
        List<File> fileList = fileChooser2.showOpenMultipleDialog(primaryStage);
        try {
            for (File file : fileList) {
                imagePath.add(file.toPath());
                Image newImage = new Image(file.toURI().toString(),
                        AlbumFX.getwSize()/6.5, 230, true, true);
                fullImageList.add(newImage);
                ImageView iViewDirectory = new ImageView(newImage);
                if(check == 0){
                    imageViewList.add(iViewDirectory);
                    fullImgViewList.add(iViewDirectory);
                }
                Text bildtext = new Text(file.toPath().getFileName().toString());
                bildtext.setFill(Color.WHITE);
                bildtext.setOpacity(0);
                bildtext.setFont(new Font(13));
                imageName.add(bildtext);
                imageNameAgain.add(bildtext);
            }
        } catch (NullPointerException e){
            loggerShow.log(Level.SEVERE,"Exception occurred while loading images " + e);
        }
        loggerShow.info("method 'openMultiImage' finished");
        return imageViewList;
    }


    /**
     * method fills the gridpane with imageview objects and name of images
     * @param pane gridpane that will contain the imageview objects
     * @param images expected parameter is a list of imageview objects
     */
    public static void createImage (GridPane pane, List<ImageView> images){
        if(numImage == 0){
            pane.getColumnConstraints().clear();
            spalten.clear();
            for(int i = 0 ; i < col ; i++){
                spalten.add(new ColumnConstraints());
                spalten.get(i).setHalignment(HPos.CENTER);
                spalten.get(i).setMinWidth(AlbumFX.getwSize() / 6.5);
                spalten.get(i).setMaxWidth(AlbumFX.getwSize() / 6.5);
            }
            pane.getColumnConstraints().addAll(spalten);
        }
        int num = 0;
        for(ImageView imgV : images){
            pane.add(imgV, imageH, imageV);
            pane.add(imageName.get(numImage),imageH,imageV+1);
            if(check == 0){
                imgV.opacityProperty().set(0);
            }
            pane.setHgap(puffer);
            pane.setVgap(12);
            num++;
            numImage++;
            imageH++;
            if((num != 0) && (numImage % col == 0)){
                imageV+=2;
            }
            if(imageH == col){
                imageH = 0;
            }
        }
    }


    /**
     * updated the number of columns of the gridpane
     * @param pane gridpane that contains the imageview objects
     */
    public static void changeCol(GridPane pane){
        numImage = 0;
        imageH = 0;
        imageV = 0;
        check++;
        pane.getChildren().clear();
        Show.createImage(pane, fullImgViewList);
        check = 0;
    }


    /**
     * method create a String of informations about every image in the gridpane for the infobar at the bottom
     */
    public static void createInfoBar(){
        int x = 0;
        if(fullImageList.size() != imageViewList.size()){
            x = fullImgViewList.size()-imageViewList.size();
        }
        for(int p = x ; p < Show.fullImageList.size() ; p++) {
            Metadata metadata = null;
            String getHeight = null;
            String getWidth = null;
            String getFileSize = null;
            try {
                metadata = ImageMetadataReader.readMetadata(new File(String.valueOf(imagePath.get(p))));
            }
            catch (ImageProcessingException | IOException e){
                loggerShow.log(Level.SEVERE,"Exception occurred while reading metadata of an image" + e);
            }
            if (metadata != null) {
                for (Directory directory : metadata.getDirectories()) {
                    for (Tag tag : directory.getTags()) {
                        if(tag.getTagName().equals("Image Height")){
                            getHeight = tag.getDescription();
                        }
                        if(tag.getTagName().equals("Image Width")){
                            getWidth = tag.getDescription();
                        }
                        if(tag.getTagName().equals("File Size")){
                            getFileSize = tag.getDescription();
                        }
                    }
                    if (directory.hasErrors()) {
                        for (String error : directory.getErrors()) {
                            System.err.format("ERROR: %s", error);
                        }
                    }
                }
            }
            infoOfImage.add(Show.imagePath.get(p).toString() + "        " + getFileSize + "        " + "Auflösung: " + getWidth + " x " + getHeight);
        }
        loggerShow.info("creating information bar finished for " + infoOfImage.size() + " images");
    }



//----------------------------------------------------------------------------------------------------------------------
// get / set


    /**
     * @param x expected value for number of new images
     */
    public static void setNumImage(int x){
        numImage = x;
    }

    /**
     * @param x expected value for vertical image position
     */
    public static void setImageV(int x){
        imageV = x;
    }

    /**
     * @param x expected value for horizontal image position
     */
    public static void setImageH(int x){
        imageH = x;
    }

    /**
     * @param x expected value for size of gap between the images
     */
    public static void setPuffer(double x){
        puffer = x;
    }

    /**
     * @param x expected value for number of columns
     */
    public static void setCol(int x){
        col = x;
    }


    /**
     * @param x expected logging Level
     */
    public static void setLogLevel(Level x){
        loggerShow.setLevel(x);
    }


    /*
    public static int getNumImage(){
        return numImage;
    }
    public static int getImageV(){
        return imageV;
    }
    public static int getImageH(){
        return imageH;
    }
    public static int getCheck(){
        return check;
    }
    public static double getPuffer(){
        return puffer;
    }
    public static int getCol(){
        return col;
    }

     */

}
