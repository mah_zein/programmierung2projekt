import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.PickResult;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;

public class AlbumFX extends Application {
    private static Logger loggerAlbumFX = Logger.getLogger("AlbumFX");

    public static void main(String[] args) {
        launch(args);
    }

    private static int wSize;
    private static int hSize;
    public static ArrayList<Image> fullImageList = new ArrayList<>();
    private GridPane raster;
    private Node selectedNode;
    private ImageView selectedImg; // ausgewähltes Bild
    private ImageView bufferImg; //zuvor ausgewähltes Bild
    private ImageView clickedBufferImage = new ImageView(); //zuvor angeklicktes Bild
    private int checkClicked = 0;
    private int cnt = 0;
    private Node clickPick;
    private Popup imagePopup;
    private int nextImage = 0;
    private AnimationTimer timer2;

    /**
     * @param primaryStage main window
     */
    @Override
    public void start(Stage primaryStage) {

        Handler handler1 = null;
        try {
            handler1 = new FileHandler("log.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (handler1 != null) {
            loggerAlbumFX.addHandler(handler1);
        }


//----------------------------------------------------------------------------------------------------------------------
// create menubar

        MenuBar menuBarTop = new MenuBar();
        Menu fileMenu = new Menu("Datei");
        fileMenu.setStyle("-fx-background-color: grey");
        Menu help = new Menu("Hilfe");
        help.setStyle("-fx-background-color: grey");

        MenuItem operations = new MenuItem("Bedienung");
        help.getItems().addAll(operations);

        Menu logging = new Menu("Logging Level");
        RadioMenuItem level1 = new RadioMenuItem("FINEST");
        RadioMenuItem level2 = new RadioMenuItem("FINER");
        RadioMenuItem level3 = new RadioMenuItem("FINE");
        RadioMenuItem level4 = new RadioMenuItem("CONFIG");
        RadioMenuItem level5 = new RadioMenuItem("INFO");
        RadioMenuItem level6 = new RadioMenuItem("WARNING");
        RadioMenuItem level7 = new RadioMenuItem("SEVERE");
        RadioMenuItem level8 = new RadioMenuItem("ALL");
        RadioMenuItem level9 = new RadioMenuItem("OFF");
        ToggleGroup toggleGroup = new ToggleGroup();
        toggleGroup.getToggles().add(level1);
        toggleGroup.getToggles().add(level2);
        toggleGroup.getToggles().add(level3);
        toggleGroup.getToggles().add(level4);
        toggleGroup.getToggles().add(level5);
        toggleGroup.getToggles().add(level6);
        toggleGroup.getToggles().add(level7);
        toggleGroup.getToggles().add(level8);
        toggleGroup.getToggles().add(level9);
        logging.getItems().addAll(level1,level2,level3,level4,level5,level6,level7,level8,level9);

        MenuItem openImage = new MenuItem("Bilder hinzufügen");
        MenuItem openDirectory = new MenuItem("Ordnerinhalt hinzufügen");
        MenuItem clearAll = new MenuItem("Album leeren");
        clearAll.setAccelerator(KeyCombination.keyCombination("Ctrl+Delete"));
        MenuItem hideImageName = new MenuItem("Bildernamen ein/ausblenden");
        hideImageName.setAccelerator(KeyCombination.keyCombination("Ctrl+n"));
        MenuItem rename = new MenuItem("Bildernamen ändern");
        rename.setAccelerator(KeyCombination.keyCombination("Ctrl+r"));
        MenuItem exit = new MenuItem("Speichern & Beenden");

        fileMenu.getItems().addAll(openImage, openDirectory, hideImageName, rename, clearAll, logging, exit);
        menuBarTop.getMenus().addAll(fileMenu,help);
        menuBarTop.setBackground((new Background((new BackgroundFill(Color.GREY, CornerRadii.EMPTY, Insets.EMPTY)))));


//----------------------------------------------------------------------------------------------------------------------
// create the basic program

        primaryStage.setTitle("Album");
        raster = new GridPane();
        BorderPane gesamt = new BorderPane();
        Scene scene = new Scene(gesamt, wSize * 0.8, hSize * 0.8);
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.show();
        wSize = (int) primaryStage.getWidth();
        hSize = (int) primaryStage.getHeight();
        primaryStage.setMinWidth(wSize / 2.0);
        Show.setPuffer(wSize * 0.01); //nur am Anfang: Gap zwischen Bildern(ImageView Objekte)
        raster.setPadding(new Insets(wSize * 0.007));
        ScrollPane scrollpane = new ScrollPane();
        scrollpane.setContent(raster);
        gesamt.setStyle("-fx-background: black");
        scrollpane.setStyle("-fx-background: black");
        scrollpane.setStyle("-fx-focus-color: grey");
        raster.setMaxWidth(wSize);
        scrollpane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollpane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        Text infoBar = new Text();
        infoBar.setFill(Color.WHITE);

        gesamt.setTop(menuBarTop);
        gesamt.setCenter(scrollpane);
        gesamt.setBottom(infoBar);

        AnimationTimer timer = new MyTimer(); //Effekt für das Einblenden der Bilder
        timer2 = new MyTimer2();

        loggerAlbumFX.info("program started");


//----------------------------------------------------------------------------------------------------------------------
// load the saved images

        FileReader fr= null;
        try {
            fr = new FileReader("savedImages.txt");
        } catch (FileNotFoundException e) {
            loggerAlbumFX.log(Level.SEVERE, "Exception occurred while reading the saved text file " + e);
        }
        BufferedReader br = null;
        try {
            if (fr != null) {
                br = new BufferedReader(fr);
            }
        }
        catch (NullPointerException e){
            loggerAlbumFX.log(Level.SEVERE, "Exception occurred while reading the saved text file " + e);
        }
        String k = null;
        try {
            if(br != null){
                k = br.readLine();
            }
        } catch (IOException e) {
            loggerAlbumFX.log(Level.SEVERE, "Exception occurred while reading the saved text file " + e);
        }
        while(!(k == null)){
            try {
                Show.imagePath.add(Paths.get(k));
                File file = new File(String.valueOf(Paths.get(k)));
                Image newImage= new Image(file.toURI().toString(),
                        AlbumFX.getwSize()/6.5, 230, true, true);
                fullImageList.add(newImage);
                Text bildtext = new Text(file.toPath().getFileName().toString());
                bildtext.setFill(Color.WHITE);
                bildtext.setOpacity(0);
                bildtext.setFont(new Font(13));
                Show.imageName.add(bildtext);
                Show.imageNameAgain.add(bildtext);
                ImageView iViewDirectory = new ImageView(newImage);
                Show.fullImgViewList.add(iViewDirectory);
                Show.imageViewList.add(iViewDirectory);
                k = br.readLine();
            } catch (IOException e) {
                loggerAlbumFX.log(Level.SEVERE, "Exception occurred while reading the saved text file " + e);
            }
        }
        try {
            if(br != null){
                br.close();
            }
        } catch (IOException e) {
            loggerAlbumFX.log(Level.SEVERE, "Exception occurred while closing the BufferedReader stream " + e);
        }
        try {
            if(fr != null){
                fr.close();
            }
        } catch (IOException e) {
            loggerAlbumFX.log(Level.SEVERE, "Exception occurred while closing the FileReader stream " + e);
        }

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Show.createImage(raster, Show.fullImgViewList);
                Thread infoBarThread = new Thread(new ImageInfoBar());
                infoBarThread.start();
            }
        });
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (Show.fullImgViewList.size() != 0) {
                    timer.start();
                }
            }
        });

        loggerAlbumFX.info("number of loaded images from savedImages.txt: " + Show.imagePath.size());

//----------------------------------------------------------------------------------------------------------------------
// events/listener for mouseclick / mousemove / pressed keys / menuitems / change window size

        raster.setOnMouseMoved(eventtt -> {  // Effekt beim berühren eines Bildes mit dem Cursor
            if (fullImageList.size() != 0) {
                for (int i = 0; i < fullImageList.size(); i++) {
                    Show.fullImgViewList.get(i).setOnMouseEntered(event -> {
                        PickResult x = event.getPickResult();
                        selectedNode = x.getIntersectedNode();
                        selectedImg = (ImageView) selectedNode;
                        if ((selectedImg.getClass().equals(ImageView.class) && bufferImg != selectedImg)) {
                            selectedImg.setStyle("-fx-scale-x: 1.035; -fx-scale-y: 1.035");
                            bufferImg = selectedImg;
                        }
                    });
                    Show.fullImgViewList.get(i).setOnMouseExited(event -> {
                        if (selectedImg != null) {
                            selectedImg.setStyle("-fx-scale-y: 1; -fx-scale-x: 1");
                        }
                    });
                }
            }
        });

        raster.setOnMouseClicked(event -> {  // Effekt beim anklicken eines Bildes
            PickResult clickedPickResult = event.getPickResult();  //speichert das ausgewählte Element in einen Behälter
            clickPick = clickedPickResult.getIntersectedNode();  //get.IntersectNode() -> Node wird in clickPick gespeichert

            if (!clickPick.getClass().equals(GridPane.class) && !clickPick.getClass().equals(Text.class)) {  // Prüfung ob das clickPick-Element ein ImageView Objekt ist
                for (int i = 0; i < fullImageList.size(); i++) {
                    if ((ImageView) clickPick == Show.fullImgViewList.get(i)) { // prüft welches ImageViewList-Element angeklickt wurde (clickPick)
                        if (checkClicked == 0) {  // kein bild ist angeklickt
                            for (int j = 0; j < fullImageList.size(); j++) {
                                Show.fullImgViewList.get(j).setOpacity(0.4);
                                Show.imageName.get(j).setFill(Color.GREY);
                            }
                            infoBar.setText(Show.infoOfImage.get(i));
                            Show.fullImgViewList.get(i).setOpacity(1);
                            Show.imageName.get(i).setFill(Color.WHITE);
                            checkClicked = 1;
                            loggerAlbumFX.info("imageview object selected - list index: " + i);
                        } else if ((checkClicked == 1) && (clickedBufferImage == Show.fullImgViewList.get(i))) {  // das angeklickte bild wird nochmal angeklickt
                            ImageView presentation = new ImageView(new Image(Show.imagePath.get(i).toUri().toString(),
                                    wSize - 14.66, hSize - 14.66, true, true));
                            HBox imageBox = new HBox();
                            imageBox.setMinSize(wSize - 13.66, hSize - 14.66);
                            imageBox.setMaxSize(wSize - 13.66, hSize - 14.66);
                            imageBox.setStyle("-fx-background-color: rgba(0, 0, 0, .55)");
                            imageBox.getChildren().addAll(presentation);
                            imageBox.setAlignment(Pos.CENTER);
                            imagePopup = new Popup();
                            imagePopup.getContent().addAll(imageBox);
                            imagePopup.show(primaryStage);
                            checkClicked = 2;
                            loggerAlbumFX.info("preview an image");

                            int finalI = i;
                            scene.setOnKeyPressed(event2 -> {
                                if (event2.getCode() == KeyCode.ENTER) {  // bei der Bildvorschau wird 'Enter' gedrückt
                                    if (finalI + nextImage < Show.fullImgViewList.size() - 1) {
                                        nextImage++;
                                        ImageView newI = new ImageView(new Image(Show.imagePath.get(finalI + nextImage).toUri().toString(),
                                                wSize - 14.66, hSize - 14.66, true, true));
                                        imageBox.getChildren().clear();
                                        imageBox.getChildren().add(newI);
                                    }
                                }
                                if (event2.getCode() == KeyCode.BACK_SPACE) {  //  bei der Bildvorschau wird 'Backspace' gedrückt
                                    if (finalI + nextImage > 0) {
                                        nextImage--;
                                    }
                                    ImageView newI = new ImageView(new Image(Show.imagePath.get(finalI + nextImage).toUri().toString(),
                                            wSize - 14.66, hSize - 14.66, true, true));
                                    imageBox.getChildren().clear();
                                    imageBox.getChildren().add(newI);
                                }
                            });
                            nextImage = 0;
                        } else if ((checkClicked == 2) && (clickedBufferImage == Show.fullImgViewList.get(i))) {  //ein bild wird zum 3. mal angeklickt
                            for (int j = 0; j < fullImageList.size(); j++) {
                                Show.fullImgViewList.get(j).setOpacity(1);
                                Show.imageName.get(j).setFill(Color.WHITE);
                            }
                            infoBar.setText("");
                            checkClicked = 0;
                            loggerAlbumFX.info("image is deselected");
                        } else if (((checkClicked == 1) || (checkClicked == 2)) && (clickedBufferImage != Show.fullImgViewList.get(i))) {  // ein anderes bild wird angeklickt
                            Show.fullImgViewList.get(i).setOpacity(1);
                            for (int h = 0; h < Show.imageName.size(); h++) {
                                Show.imageName.get(h).setFill(Color.GREY);
                            }
                            Show.imageName.get(i).setFill(Color.WHITE);
                            clickedBufferImage.setOpacity(0.4);
                            infoBar.setText(Show.infoOfImage.get(i));
                            checkClicked = 1;
                            loggerAlbumFX.info("another imageview object selected - list index: " + i);
                        }
                        clickedBufferImage = Show.fullImgViewList.get(i);
                    }
                }
            }
            else { // mal klickt auf ein Text / Gridpane
                for (int j = 0; j < fullImageList.size(); j++) {
                    Show.fullImgViewList.get(j).setOpacity(1);
                    for (int h = 0; h < Show.imageName.size(); h++) {
                        Show.imageName.get(h).setFill(Color.WHITE);
                    }
                    checkClicked = 0;
                    infoBar.setText("");
                }
                loggerAlbumFX.info("no image selected");
            }
        });

        //https://blog.idrsolutions.com/2012/11/adding-a-window-resize-listener-to-javafx-scene/
        scene.widthProperty().addListener(new ChangeListener<Number>() {   //  passt je nach Fenstergröße die Spaltenanzahl und den Spaltenabstand an
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth) {
                if (newSceneWidth.doubleValue() > (wSize - 25)) {
                    Show.setPuffer(wSize * 0.01);
                    Show.setCol(6);
                    Show.changeCol(raster);
                }
                if (newSceneWidth.doubleValue() <= (wSize - 25) && newSceneWidth.doubleValue() >= wSize * 0.84) {
                    Show.setPuffer((newSceneWidth.doubleValue() - (wSize * 0.84)) / 4 + (wSize * 0.0129));
                    Show.setCol(5);
                    Show.changeCol(raster);
                }
                if (newSceneWidth.doubleValue() < (wSize * 0.84) && newSceneWidth.doubleValue() >= (wSize * 0.68)) {
                    Show.setPuffer((newSceneWidth.doubleValue() - (wSize * 0.68)) / 3 + (wSize * 0.0154));
                    Show.setCol(4);
                    Show.changeCol(raster);
                }
                if (newSceneWidth.doubleValue() < (wSize * 0.68) && newSceneWidth.doubleValue() >= (wSize * 0.49)) {
                    Show.setPuffer((newSceneWidth.doubleValue() - (wSize * 0.515)) / 2 + (wSize * 0.0181));
                    Show.setCol(3);
                    Show.changeCol(raster);
                }
            }
        });

        operations.setOnAction(event -> {
            Popup popup = new Popup();
            Button button1 = new Button("Schließen");
            VBox helpBox = new VBox();
            helpBox.setAlignment(Pos.CENTER);
            Text info = new Text("Bedienung");
            Text info2 = new Text();
            info2.setText("In der Bildervorschau:\n'Enter' - Anzeigen des nächsten Bildes\n'Backspace' - Anzeigen des vorherigen Bildes\n" +
                    "'Esc' - Schließen der Bildervorschau\n \nAuswahl eines Bilder:\n1.Klick - Bild wird augewählt\n2.Klick - Bildervorschau\n" +
                    "3.Klick / Klick auf den Hintergrund - Bild wird abgewählt");
            info2.wrappingWidthProperty().set(wSize/3.5);
            info.setFont(new Font(18));
            info2.setFont(new Font(16));
            helpBox.setFillWidth(true);
            helpBox.setPadding(new Insets(12));
            helpBox.setStyle("-fx-background-color: white");
            helpBox.getChildren().addAll(info,info2,button1);
            helpBox.setSpacing(10);
            popup.getContent().addAll(helpBox);
            popup.show(primaryStage);

            button1.setOnAction(event1 -> {
                popup.hide();
            });
        });

        level1.setOnAction(event -> {
            loggerAlbumFX.setLevel(Level.FINEST);
            Show.setLogLevel(Level.FINEST);
        });

        level2.setOnAction(event -> {
            loggerAlbumFX.setLevel(Level.FINER);
            Show.setLogLevel(Level.FINER);
        });

        level3.setOnAction(event -> {
            loggerAlbumFX.setLevel(Level.FINE);
            Show.setLogLevel(Level.FINE);
        });

        level4.setOnAction(event -> {
            loggerAlbumFX.setLevel(Level.CONFIG);
            Show.setLogLevel(Level.CONFIG);
        });

        level5.setOnAction(event -> {
            loggerAlbumFX.setLevel(Level.INFO);
            Show.setLogLevel(Level.INFO);
        });

        level6.setOnAction(event -> {
            loggerAlbumFX.setLevel(Level.WARNING);
            Show.setLogLevel(Level.WARNING);
        });

        level7.setOnAction(event -> {
            loggerAlbumFX.setLevel(Level.SEVERE);
            Show.setLogLevel(Level.SEVERE);
        });

        level8.setOnAction(event -> {
            loggerAlbumFX.setLevel(Level.ALL);
            Show.setLogLevel(Level.ALL);
        });

        level9.setOnAction(event -> {
            loggerAlbumFX.setLevel(Level.OFF);
            Show.setLogLevel(Level.OFF);
        });


        hideImageName.setOnAction(event -> {  //  blendet die Bildernamen ein/aus
            if (cnt == 0) {
                for (Text txt : Show.imageName) {
                    txt.setOpacity(0);
                }
                cnt = 1;
                loggerAlbumFX.info("hide image names");
            } else if (cnt == 1) {
                for (Text txt : Show.imageName) {
                    txt.setOpacity(1);
                }
                cnt = 0;
                loggerAlbumFX.info("show image names");
            }
        });

        rename.setOnAction(event -> {  // Bildername wird geändert
            if (clickPick != null) {
                if (clickPick.getClass() == ImageView.class) {
                    Popup popup = new Popup();
                    TextField text = new TextField();
                    Button button1 = new Button("Enter");
                    Button button2 = new Button("Abbrechen");
                    VBox renameBox1 = new VBox();
                    HBox renameBox2 = new HBox();
                    renameBox2.setAlignment(Pos.CENTER);
                    renameBox1.setAlignment(Pos.CENTER);
                    Text info = new Text("Neuen Bildernamen eintragen");
                    info.setFont(new Font(18));
                    renameBox2.setFillHeight(true);
                    renameBox2.setPadding(new Insets(12));
                    renameBox1.setPadding(new Insets(12));
                    renameBox1.setStyle("-fx-background-color: grey");
                    renameBox2.getChildren().addAll(text, button1, button2);
                    renameBox1.getChildren().addAll(info, renameBox2);
                    renameBox2.setSpacing(10);
                    popup.getContent().addAll(renameBox1);
                    popup.show(primaryStage);
                    button1.setOnAction(event1 -> {
                        Show.imageName.get(Show.fullImgViewList.indexOf(clickPick)).setText(text.getCharacters().toString());
                        popup.hide();
                    });
                    button2.setOnAction(event2 -> {
                        popup.hide();
                    });
                    loggerAlbumFX.info("renamed an image");
                }
            }
        });

        clearAll.setOnAction(event -> {   //  gesamtes Album wird gelöscht
            Popup popup = new Popup();
            Button button1 = new Button("Album löschen");
            Button button2 = new Button("Abbrechen");
            VBox clearBox1 = new VBox();
            HBox clearBox2 = new HBox();
            clearBox2.setAlignment(Pos.CENTER);
            clearBox1.setAlignment(Pos.CENTER);
            Text info = new Text("Wirklich alle Bilder löschen?");
            info.setFont(new Font(18));
            clearBox2.setFillHeight(true);
            clearBox2.setPadding(new Insets(12));
            clearBox1.setPadding(new Insets(12));
            clearBox1.setStyle("-fx-background-color: grey");
            clearBox2.getChildren().addAll(button1, button2);
            clearBox1.getChildren().addAll(info, clearBox2);
            clearBox2.setSpacing(10);
            popup.getContent().addAll(clearBox1);
            popup.show(primaryStage);

            button1.setOnAction(event1 -> {
                Show.setNumImage(0);
                Show.setImageH(0);
                Show.setImageV(0);
                fullImageList.clear();
                Show.imageViewList.clear();
                Show.fullImgViewList.clear();
                raster.getChildren().clear();
                Show.imagePath.clear();
                checkClicked = 0;
                Show.imageName.clear();
                Show.imageNameAgain.clear();
                Show.infoOfImage.clear();
                infoBar.setText("");
                cnt = 0;
                popup.hide();
                loggerAlbumFX.info("all images were deleted by using the MenuItem clearAll");
            });
            button2.setOnAction(event1 -> {
                popup.hide();
            });
        });

        openImage.setOnAction(event -> {  //  einzelnes/mehrere Bilder werden geladen
            Show.imageViewList.clear();
            Show.imageNameAgain.clear();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    List<ImageView> pufferListImages = Show.openMultiImage(primaryStage);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            Show.createImage(raster, pufferListImages);
                            Thread infoBarThread = new Thread(new ImageInfoBar());
                            infoBarThread.start();

                        }
                    });
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            if (pufferListImages.size() != 0) {
                                timer.start();
                            }
                        }
                    });
                    loggerAlbumFX.info("images loaded: " + pufferListImages.size());
                    loggerAlbumFX.info("number of all images: " + fullImageList.size());
                }
            });
        });

        openDirectory.setOnAction(event -> {  //  Ordner mit Bildern wird geladen
            Show.imageViewList.clear();
            Show.imageNameAgain.clear();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    List<ImageView> pufferListDirectory = Show.openDirectory(primaryStage);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            Show.createImage(raster, pufferListDirectory);
                            Thread infoBarThread = new Thread(new ImageInfoBar());
                            infoBarThread.start();
                        }
                    });
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            if (pufferListDirectory.size() != 0) {
                                timer.start();
                            }
                        }
                    });
                    loggerAlbumFX.info("images loaded: " + pufferListDirectory.size());
                    loggerAlbumFX.info("number of all images: " + fullImageList.size());
                }
            });
        });

        exit.setOnAction(event -> {  // speichert Bilder und beendet das programm

            PrintWriter printWriter = null;

            try {
                printWriter = new PrintWriter("savedImages.txt");
            } catch (FileNotFoundException e) {
                loggerAlbumFX.log(Level.SEVERE,"Exception occurred while creating the text file " + e);
            }

            for(int i = 0; i < Show.imagePath.size(); i++ ){
                if (printWriter != null) {
                    printWriter.println(Show.imagePath.get(i).toString());
                }
            }

            if (printWriter != null) {
                printWriter.flush();
            }

            loggerAlbumFX.info("program ended - saved images: " + Show.imagePath.size());

            Platform.exit();
            System.exit(0);
        });
    }


//----------------------------------------------------------------------------------------------------------------------
// get / set

    /**
     * @return get the width of the primaryStage
     */
    public static int getwSize() {
        return wSize;
    }

    /**
     * @return get the height of the primaryStage
     */
    public static int gethSize() {
        return hSize;
    }

    /**
     * class for fade in the iamges
     */
    private class MyTimer extends AnimationTimer { //  Animation Einblenden der Bilder & Bildernamen
        int i = 0;
        int f = 1;
        int g = 2;
        int h = 3;
        int cnt = 0;
        double opacity1 = 0;
        double opacity2 = 0;
        double opacity3 = 0;
        double opacity4 = 0;

        @Override
        public void handle(long now) {
            doHandle();
        }

        /**
         * set increasing opacity of the images
         */
        private void doHandle() {
            if (i < Show.imageViewList.size()) {
                opacity1 = opacity1 + 0.05;
                Show.imageViewList.get(i).opacityProperty().set(opacity1);
                Show.imageNameAgain.get(i).setOpacity(opacity1);
                imgSizeStart(Show.imageViewList.get(i), opacity1);
            }
            if (cnt >= 5 && f < Show.imageViewList.size()) {
                opacity2 = opacity2 + 0.05;
                Show.imageViewList.get(f).opacityProperty().set(opacity2);
                Show.imageNameAgain.get(f).setOpacity(opacity2);
                imgSizeStart(Show.imageViewList.get(f), opacity2);
            }
            if (cnt >= 10 && g < Show.imageViewList.size()) {
                opacity3 = opacity3 + 0.05;
                Show.imageViewList.get(g).opacityProperty().set(opacity3);
                Show.imageNameAgain.get(g).setOpacity(opacity3);
                imgSizeStart(Show.imageViewList.get(g), opacity3);
            }
            if (cnt >= 15 && h < Show.imageViewList.size()) {
                opacity4 = opacity4 + 0.05;
                Show.imageViewList.get(h).opacityProperty().set(opacity4);
                Show.imageNameAgain.get(h).setOpacity(opacity4);
                imgSizeStart(Show.imageViewList.get(h), opacity4);
            }
            if (opacity4 >= 1 && h <= Show.imageViewList.size()) {
                h = h + 4;
                opacity4 = 0;
            }
            if (opacity3 >= 1 && g <= Show.imageViewList.size()) {
                g = g + 4;
                opacity3 = 0;
            }
            if (opacity2 >= 1 && f <= Show.imageViewList.size()) {
                f = f + 4;
                opacity2 = 0;
            }
            if (opacity1 >= 1 && i <= Show.imageViewList.size()) {
                i = i + 4;
                opacity1 = 0;
            }
            if (g >= Show.imageViewList.size() && f >= Show.imageViewList.size()
                    && i >= Show.imageViewList.size() && h >= Show.imageViewList.size()) {
                opacity1 = 0;
                opacity2 = 0;
                opacity3 = 0;
                opacity4 = 0;
                cnt = 0;
                i = 0;
                f = 1;
                g = 2;
                h = 3;
                loggerAlbumFX.info("fade in animation for images + image names finished");
                stop();
                timer2.start();

            } else {
                cnt++;
            }
        }

        /**
         * @param imgV Imageview object that gets the right size
         * @param x check value for animation
         */
        private void imgSizeStart(ImageView imgV, double x) {
            if (x > 0.09 && x < 0.11) {
                imgV.setStyle("-fx-scale-x: 1.18 ; -fx-scale-y: 1.18");
            }
            if (x > 0.19 && x < 0.21) {
                imgV.setStyle("-fx-scale-x: 1.16 ; -fx-scale-y: 1.16");
            }
            if (x > 0.29 && x < 0.31) {
                imgV.setStyle("-fx-scale-x: 1.14 ; -fx-scale-y: 1.14");
            }
            if (x > 0.39 && x < 0.41) {
                imgV.setStyle("-fx-scale-x: 1.12 ; -fx-scale-y: 1.12");
            }
            if (x > 0.49 && x < 0.51) {
                imgV.setStyle("-fx-scale-x: 1.1 ; -fx-scale-y: 1.1");
            }
            if (x > 0.59 && x < 0.61) {
                imgV.setStyle("-fx-scale-x: 1.08 ; -fx-scale-y: 1.08");
            }
            if (x > 0.69 && x < 0.71) {
                imgV.setStyle("-fx-scale-x: 1.06 ; -fx-scale-y: 1.06");
            }
            if (x > 0.79 && x < 0.81) {
                imgV.setStyle("-fx-scale-x: 1.04 ; -fx-scale-y: 1.04");
            }
            if (x > 0.89 && x < 0.91) {
                imgV.setStyle("-fx-scale-x: 1.02 ; -fx-scale-y: 1.02");
            }
            if (x > 0.99) {
                imgV.setStyle("-fx-scale-x: 1 ; -fx-scale-y: 1");
            }
        }
    }

    /**
     * additionally animation
     */
    private class MyTimer2 extends AnimationTimer {  // Animation Ausblenden der Bildernamen
        double opacity = 1;
        @Override
        public void handle(long now){
            doHandle();
        }

        /**
         * set increasing opacity of imagename
         */
        private void doHandle(){
            opacity = opacity - 0.02;
            for(int x = 0 ; x < Show.imageNameAgain.size() ; x++) {
                Show.imageNameAgain.get(x).setOpacity(opacity);
            }
            if(opacity <= 0){
                cnt = 1;
                opacity = 1;
                loggerAlbumFX.info("hide animation for image names finished");
                stop();
            }
        }
    }

}
